# LikeLibBDK #



### What is LikeLibBDK? ###

**LikeLibBDK** is a blockchain development kit for **LikeLib** that contains documentation, sample code, extensions, 
python APIs, smart contracts, wallet templates, and analytical & monitoring tools.